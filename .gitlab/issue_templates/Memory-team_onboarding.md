<!-- Title: Memory team onboarding: [New team member name] -->

## [First Name], Welcome to GitLab and the Memory Team!

We are all excited that you are joining us on the [Memory Team](https://about.gitlab.com/handbook/engineering/development/enablement/memory/).  You should have already received an onboarding issue from [People Ops](https://about.gitlab.com/handbook/people-operations/) familiarizing yourself with GitLab, setting up accounts, accessing tools, etc.  This onboarding issue is specific to the Memory Team people, processes and setup.

For the first week or so you should focus on your GitLab onboarding issue.  There is a lot of information there, and the first week is very important to get your account information set up correctly.  Tasks from this issue can start as you get ready to start contributing to the Memory team.

Much like your GitLab onboarding issue, each item is broken out by Owner: Action.  Just focus on "New Team Member" items, and feel free to reach out to your team if you have any questions.

### First Day

* [ ] Manager: Invite team member to [#g_memory](https://gitlab.slack.com/archives/CGN8BUCKC) Slack Channel
* [ ] Manager: Invite team member to Monday weekly Memory Team Meeting
* [ ] Manager: Add new team member to [Memory Team Retro](https://gitlab.com/gl-retrospectives/memory-team/-/project_members)

### First Week

* [ ] Manager: Add new team member to [geekbot standup](https://geekbot.com/dashboard/)
* [ ] Manager: Add new team member to Slack memory-team user group
* [ ] Manager: Add new team member introduction to the [Engineering Week In Review](https://docs.google.com/document/d/1Oglq0-rLbPFRNbqCDfHT0-Y3NkVEiHj6UukfYijHyUs/edit).
* [ ] Manager: Add new team member to relevant [office hours](https://gitlab.com/gitlab-org/memory-team/team-tasks/issues/1)
* [ ] New team member: Read about your team, its mission, team members and resources on the [Memory Team page](https://about.gitlab.com/handbook/engineering/development/enablement/memory/)
* [ ] New team member: Set up [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to meet your team

### Second Week

* [ ] New team member: Read about [how GitLab uses labels](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#labels), and the [label definitions](https://gitlab.com/gitlab-org/gitlab/-/labels)
* [ ] New team member: Familiarize yourself with the team boards
  * [ ] [Memory build board: What we are working on](https://gitlab.com/groups/gitlab-org/-/boards/2333522?label_name[]=group%3A%3Amemory&label_name[]=memory%3A%3Aactive)
  * [ ] [Memory validation board: Ideas we are evaluating](https://gitlab.com/groups/gitlab-org/-/boards/2334157?scope=all&utf8=%E2%9C%93&lab[%E2%80%A6]el_name[]=group%3A%3Amemory&label_name[]=memory%3A%3Avalidation&label_name[]=group%3A%3Amemory)
* [ ] New team member: Watch [Memory Team Educational Videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KphaTLuumYvZMbUECd6zFlT).  Note that these videos are private and can only be viewed with the GitLab Unfiltered YouTube account.  Contact [People Ops](https://about.gitlab.com/handbook/people-operations/) if you don't already have access.
* [ ] New team member: Please review this onboarding issue and update the template with some improvements as you see fit to make it easier for the next team member who joins us!

### Processes

Unless covered by the company onboarding issue already, team members should familiarize themselves with the
following processes.

#### Product Development Processes

Listed in the order of relevance from inception to shipping a change:

* [RICE framework: How we decide what to prioritize](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework)
* [Issue workflow](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html)
* [Engineering workflow: General practices for contributing changes to GitLab](https://about.gitlab.com/handbook/engineering/workflow/)
* [Testing best practices](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html)
* [Feature flags: How changes are made safely](https://docs.gitlab.com/ee/development/feature_flags/index.html)
* [Code review process: How changes are merged](https://docs.gitlab.com/ee/development/code_review.html)
* [Release process: How changes are released to GitLab.com](https://about.gitlab.com/handbook/engineering/releases/)

#### Important Dates

* [Important dates PMs should keep in mind](https://about.gitlab.com/handbook/product/product-manager-role/#important-dates-pms-should-keep-in-mind)
* [Product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline)

### Useful Links

* [Performance Guidelines](https://docs.gitlab.com/ee/development/performance.html)
* [Videos: Monitoring at GitLab](https://www.youtube.com/playlist?list=PL05JrBw4t0KpQMEbnXjeQUA22SZtz7J0e)
* [Memory Team: Knowledge Sharing and Lessons Learned](https://about.gitlab.com/handbook/engineering/development/enablement/memory/#knowledge-sharing-and-lessons-learned)
* Team blog posts:
  * [Why we created a memory team](https://about.gitlab.com/blog/2019/09/13/why-we-created-the-gitlab-memory-team/)
  * [How we migrated to Puma](https://about.gitlab.com/blog/2020/07/08/migrating-to-puma-on-gitlab/)
  * [Understand and debug problems with heap compaction](https://about.gitlab.com/blog/2021/04/28/puma-nakayoshi-fork-and-compaction/)
  * [How we built an image-scaler](https://about.gitlab.com/blog/2020/11/02/scaling-down-how-we-prototyped-an-image-scaler-at-gitlab/)

---

/confidential
/due in 21 days
